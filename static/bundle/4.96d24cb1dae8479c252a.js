/**
 * 
 */

(window.webpackJsonp=window.webpackJsonp||[]).push([[4],{51:function(e,t,n){"use strict";n.r(t),n.d(t,"mount",function(){return r});var o=null,r=function(){var e=document.querySelector("[data-footer-reveal]");if(e){var t=function(t){t.dataset.footerReveal&&t.style.setProperty("--footerHeight","".concat(e.firstElementChild.offsetHeight,"px"))};e.dataset.footerReveal="yes",t(e),window.ResizeObserver?(o&&o.disconnect(),(o=new ResizeObserver(function(n,o){return t(e)})).observe(e)):n.e(13).then(function(){var e=n(35);return"object"==typeof e&&e&&e.__esModule?e:Object.assign({},"object"==typeof e&&e,{default:e})}).then(function(n){var o=n.default;o().removeAllListeners(e),o().listenTo(e,t)})}}}}]);
