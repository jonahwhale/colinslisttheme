<?php
/**
 * Colors options
 *
 * @copyright 2019-present Creative Themes
 * @license   http://www.gnu.org/copyleft/gpl.html GNU General Public License
 * @package   Blocksy
 */

$options = [
	'colors_section_options' => [
		'type' => 'ct-options',
		'setting' => [ 'transport' => 'postMessage' ],
		'inner-options' => [

			'colorPalette' => [
				'label' => __( 'Global Colors', 'blocksy' ),
				'type'  => 'ct-color-picker',
				'design' => 'block',
				'setting' => [ 'transport' => 'postMessage' ],
				'predefined' => true,
				'wrapperAttr' => [ 'data-type' => 'color-palette' ],

				'value' => [
					'color1' => [
						'color' => '#3eaf7c',
					],

					'color2' => [
						'color' => '#33a370',
					],


					'color3' => [
						'color' => 'rgba(44, 62, 80, 0.9)',
					],

					'color4' => [
						'color' => 'rgba(44, 62, 80, 1)',
					],

					'color5' => [
						'color' => '#ffffff',
					],
				],

				'pickers' => [
					[
						'title' => __( 'Color 1', 'blocksy' ),
						'id' => 'color1',
					],

					[
						'title' => __( 'Color 2', 'blocksy' ),
						'id' => 'color2',
					],

					[
						'title' => __( 'Color 3', 'blocksy' ),
						'id' => 'color3',
					],

					[
						'title' => __( 'Color 4', 'blocksy' ),
						'id' => 'color4',
					],

					[
						'title' => __( 'Color 5', 'blocksy' ),
						'id' => 'color5',
					],
				],
			],

			blocksy_rand_md5() => [
				'type' => 'ct-divider',
				'attr' => [ 'data-type' => 'small' ],
			],

			'fontColor' => [
				'label' => __( 'Font Color', 'blocksy' ),
				'type'  => 'ct-color-picker',
				'design' => 'inline',
				'setting' => [ 'transport' => 'postMessage' ],

				'value' => [
					'default' => [
						'color' => 'var(--paletteColor3)',
					],
				],

				'pickers' => [
					[
						'title' => __( 'Initial', 'blocksy' ),
						'id' => 'default',
					],
				],
			],

			'linkColor' => [
				'label' => __( 'Link Color', 'blocksy' ),
				'type'  => 'ct-color-picker',
				'design' => 'inline',
				'setting' => [ 'transport' => 'postMessage' ],

				'value' => [
					'default' => [
						'color' => 'var(--paletteColor3)',
					],

					'hover' => [
						'color' => 'var(--paletteColor1)',
					],
				],

				'pickers' => [
					[
						'title' => __( 'Initial', 'blocksy' ),
						'id' => 'default',
					],

					[
						'title' => __( 'Hover', 'blocksy' ),
						'id' => 'hover',
					],
				],
			],

			'buttonColor' => [
				'label' => __( 'Button Color', 'blocksy' ),
				'type'  => 'ct-color-picker',
				'design' => 'inline',
				'setting' => [ 'transport' => 'postMessage' ],

				'value' => [
					'default' => [
						'color' => 'var(--paletteColor1)',
					],

					'hover' => [
						'color' => 'var(--paletteColor2)',
					],
				],

				'pickers' => [
					[
						'title' => __( 'Initial', 'blocksy' ),
						'id' => 'default',
					],

					[
						'title' => __( 'Hover', 'blocksy' ),
						'id' => 'hover',
					],
				],
			],


			blocksy_rand_md5() => [
				'type' => 'ct-divider',
				'attr' => [ 'data-type' => 'small' ],
			],

			'siteBackground' => [
				'label' => __( 'Site Background', 'blocksy' ),
				'type'  => 'ct-color-picker',
				'design' => 'inline',
				'setting' => [ 'transport' => 'postMessage' ],

				'value' => [
					'default' => [
						'color' => '#59b5c2',
					],
				],

				'pickers' => [
					[
						'title' => __( 'Initial', 'blocksy' ),
						'id' => 'default',
					],
				],
			],

		],
	],
];
