<header class="site-header">

    <div class="mobile-header-nav d-lg-none mobile" style="margin-top:5px">
        <center>
            <span class="left" style="float:left;margin-left:20px;">
                <a
                class='btn btn-nav uk-button uk-button-default text-uppercase h2'
                href="/?page_id=139"
                style=""
                >
                <nobr>
                    Contact Us
                </nobr>
                </a>
            </span>
            <span class="right" style="float:right;margin-right:20px;">
            <a
                class='btn btn-nav menu-item text-uppercase h2'
                href="/?page_id=137"
            >
                <nobr>About Us</nobr>
            </a>
            </span>
        </center>
    </div>

    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm " style='max-width:300px'>
                <center>
                    <a class='btn btn-nav uk-button uk-button-default text-uppercase h2 d-none d-lg-block' href="/?page_id=139" style="">
                        <nobr>
                            Contact Us
                        </nobr>
                    </a>
                </center>
            </div>
            <div class="col-sm">
                <center>
                    <a href='/'>
                        <img style='max-width:140px' src="<?= get_template_directory_uri() ?>/img/colins-list-logo.png?v=2019"
                        class="custom-logo ct-default-logo" alt="Colin's List" />
                    </a>
                    <h6 class="subtitle" style='color:#FFF;padding-top:5px;font-family:Lato;'>
                        <i><?= get_bloginfo('description'); ?></i>
                    </h6>
                </center>
            </div>
            <div class="col-sm" style='max-width:300px'>
                <center>
                    <a
                        class='btn btn-nav menu-item text-uppercase h2 d-none d-lg-block'
                        href="/?page_id=137"
                    >
                        <nobr>About Us</nobr>
                    </a>
                </center>
            </div>
        </div>
    </div>
</header>
