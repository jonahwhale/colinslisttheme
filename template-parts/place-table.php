<table class='table table-hover' style='background:#FFF;max-width:900px;'>
<thead></thead>
<tbody>

<?php
foreach($places AS $post_id => $place) {
    ?>
    <tr id="titleRow<?= $place->getId(); ?>"
        data-toggle="collapse"
        data-target="#detailColumn<?= $place->getId(); ?>"
        aria-expanded="false"
        aria-controls="detailColumn<?= $place->getId(); ?>"
        >

        <td style="max-width:180px;min-width:110px;" align="center">

            <?= $place->showDistance(); ?>
            <span style='float:right'><?= $place->showDrinks(); ?></span>
            <br clear='all' />

        </td>
        <td width="60px" align="center">
            <?= $place->showColinsList(); ?>
        </td>
        <td style='max-width:400px;'>
            <h5>
                <!-- <a href="/?p=<?= $place->getId(); ?>"><?= $place->getName(); ?></a> -->
                <a
                href="#detailColumn<?= $place->getId(); ?>"
                data-toggle="collapse"
                data-target="#detailColumn<?= $place->getId(); ?>"
                aria-expanded="false"
                aria-controls="detailColumn<?= $place->getId(); ?>"
                >
                    <?= $place->getName(); ?>
                </a>
            </h5>
            <div><?= $place->getDescription(); ?></div>
            <!-- <div class="d-none d-lg-block"><?= $place->getAddress(); ?></div> -->
        </td>
    </tr>

    <tr  class="collapse hide" id="detailColumn<?= $place->getId(); ?>" aria-labelledby="titleRow<?= $place->getId(); ?>">
        <td style="max-width:180px;min-width:110px;border-top-color:#FFF;">
            <?= $place->showPicture(); ?>
        </td>
        <td colspan="2" style="border-top-color:#FFF;">
            <div class='badge badge-warning'<?= $place->showNeighborhood(); ?></div>
            <?= $place->showTip(); ?>

            <?= $place->showAddress(); ?>

            <i><?= $place->showWhyNot(); ?></i>

            <div class='socials'>
            <?= $place->showWebsite(); ?>
            <?= $place->showFacebook(); ?>
            <?= $place->showInstagram(); ?>
            <?= $place->showTwitter(); ?>
            </div>
        </td>
    </tr>
    <?php

}
?>


</tbody>
</table>
