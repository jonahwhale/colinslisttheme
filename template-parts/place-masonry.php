<?php
    ?>
<center>
    <div class="grid" data-masonry='{ "itemSelector": ".grid-item" }' style='margin:0 auto;'><?php
    foreach($places as $post_id => $place) { ?>
        <div class="grid-item" style='width:340px;'>
        <div class="card shadow hover "
            data-aos="zoom-in"
            data-aos-delay="200"
            style="max-width:380px; min-height:180px; margin: 10px;"
        >
            <div class="card-body">
            <div id="titleRow<?= $place->getId(); ?>"
                data-toggle="collapse"
                data-target="#detailColumn<?= $place->getId(); ?>"
                aria-expanded="false"
                aria-controls="detailColumn<?= $place->getId(); ?>"
                >
                <div class="row">
                    <div class="col">
                        <?php if($place->distance) { ?>
                            <div class="btn btn-secondary" ><?= $place->showDistance(); ?></div>
                        <?php } ?>
                    </div>
                    <div class="col">
                      <?= $place->showColinsList(); ?>
                    </div>
                    <div class="col">
                        <?php if($place->distance) { ?>
                            <div class="btn btn-secondary" title="<?= $place->getNeighborhood(); ?>"
                                style="margin:2px;white-space:nowrap;overflow: hidden;text-overflow: ellipsis;max-width:80px;">
                                <?= $place->showNeighborhood(); ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div >
                    <h5>
                        <!-- <a href="/?p=<?= $place->getId(); ?>"><?= $place->getName(); ?></a> -->
                        <a
                        href="#detailColumn<?= $place->getId(); ?>"
                        data-toggle="collapse"
                        data-target="#detailColumn<?= $place->getId(); ?>"
                        aria-expanded="false"
                        aria-controls="detailColumn<?= $place->getId(); ?>"
                        >
                            <?= $place->getName(); ?>
                        </a>
                    </h5>
                    <div><?= $place->getDescription(); ?></div>
                    <?= $place->showDrinks(); ?>

                </div>
            </div>
            <div  class="collapse hide" id="detailColumn<?= $place->getId(); ?>" aria-labelledby="titleRow<?= $place->getId(); ?>">


                <div >

                    <?= $place->showTip(); ?>

                    <?= $place->showAddress(); ?>

                    <i><?= $place->showWhyNot(); ?></i>

                    <div class='socials'>
                    <?= $place->showWebsite(); ?>
                    <?= $place->showFacebook(); ?>
                    <?= $place->showInstagram(); ?>
                    <?= $place->showTwitter(); ?>
                    </div>
                </div>
                <img class="card-img-bottom" src="<?= $place->getPicture(); ?>" >
            </div>
            </div>
        </div>
        </div><?php

    }
?></div></center><?php
