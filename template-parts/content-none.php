<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blocksy
 */

?>
<section class="no-resultsx not-foundx">
	<header class="page-headerx">
		<h1 class="page-titlex"></h1>
	</header><!-- .page-header -->

	<div class="page-contentx">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) :

		elseif ( is_search() ) :
			?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'blocksy' ); ?></p>
			<?php
			get_search_form();

		else :
            /*
			?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'blocksy' ); ?></p>
			<?php
			get_search_form();
            */
		endif;
		?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
