<?php
$numberOfColumns = 3;
$bootstrapColWidth = 12 / $numberOfColumns ;
$count = 0;
$arrayChunks = array_chunk($places, $numberOfColumns);
$placesCount = count($places);

?><span id="results"></span>

<?php if($places) {
    ?><div class="btn btn-nav" id="searchAgain">Search Again <i class="fas fa-caret-up"></i></div><?php
}
?>
<script>
$(document).ready(function() {
    $('html, body').delay(400).animate({ scrollTop: $('#results').offset().top }, 1000);
    $( "#searchAgain" ).click(function() {
        $('html, body').delay(4).animate({ scrollTop: $('#main-container').offset().top }, 500);
        // this.hide();
        $( "#searchAgain" ).fadeOut();
     });

});
</script>
<?php
// dump($rowLimit);
foreach($arrayChunks as $places) {
    $count++;
    // echo $count;

    showInlineAd();

    if($count<=$rowLimit) {
        echo '<div class="row" class="beginChunk">';
        foreach($places as $post_id => $place) {
            // dump($place->data);
            $place = new Place($place->data);
            ?><div class="col-md-<?=$bootstrapColWidth ?> column">

            <div class="card shadow hover place-card"
                data-aos="zoom-in"
                data-aos-delay="200"

            >
                <div class="card-body">
                <div id="titleRow<?= $place->getId(); ?>"
                    data-toggle="collapse"
                    data-target="#detailColumn<?= $place->getId(); ?>"
                    aria-expanded="false"
                    aria-controls="detailColumn<?= $place->getId(); ?>"
                    >
                    <div class="row">
                        <div class="col low-pad padding-top center">
                            <center><?= $place->showDrinks(); ?></center>
                        </div>
                        <div class=" low-pad">
                          <?= $place->showColinsList(); ?>
                        </div>
                        <div class="col low-pad padding-top center">
                            <center><?= $place->showEquipment(); ?></center>
                        </div>
                    </div>

                    <div >
                        <h5 class="placeName">
                            <!-- <a href="/?p=<?= $place->getId(); ?>"><?= $place->getName(); ?></a> -->
                            <a
                            href="#detailColumn<?= $place->getId(); ?>"
                            data-toggle="collapse"
                            data-target="#detailColumn<?= $place->getId(); ?>"
                            aria-expanded="false"
                            aria-controls="detailColumn<?= $place->getId(); ?>"
                            >
                                <?= $place->getName(); ?>
                            </a>
                        </h5>
                        <div><?= $place->getDescription(); ?></div>

                        <?php if($place->data->distance) { ?>
                            <div class="btn btn-secondary" ><?= $place->showDistance(); ?></div>
                        <?php } ?>
                        <?php if($place->data->distance) { ?>
                            <div class="btn btn-secondary neighborhood" title="<?= $place->getNeighborhood(); ?>"
                                style="margin:2px;white-space:nowrap;">
                                <?= $place->showNeighborhood(); ?>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div
                    class="collapse hide"
                    id="detailColumn<?= $place->getId(); ?>"
                    aria-labelledby="titleRow<?= $place->getId(); ?>">
                    <div >
                        <small><?= $place->showAddress(); ?></small>

                        <small><i><?= $place->showWhyNot(); ?></i></small>

                        <div class='socials'>
                        <?= $place->showWebsite(); ?>
                        <?= $place->showFacebook(); ?>
                        <?= $place->showInstagram(); ?>
                        <?= $place->showTwitter(); ?>
                        </div>
                    </div>
                    <?php if($place->getPicture()) { ?>
                        <img class="card-img-bottom" src="<?= $place->getPicture(); ?>" >
                    <?php } ?>
                    <br />
                    <small><i><?= $place->showTip(); ?></i></small>
                    <a
                        class="btn btn-secondary"
                        href="/contact-us/?subject=<?= $place->getName() ?>"
                        target="_new"
                    >
                        Share Tip or Make Correction
                    </a>
                </div>
                </div>
            </div> <?php
            echo '</div>';
        }
    }
    echo '</div>';

}

if($count>$rowLimit) {
    $params = $_GET;
    unset( $params['showAll'] );
    $params['showAll']   = '1';
    $showAllUrl = http_build_query( $params );
    // dump($showAllUrl);
    ?>
    <div class="site-main">
    <center>
        <a
            href="?<?= $showAllUrl ?>"
            class="btn btn-nav"

        >
            Show all</a><br /><br />
    </center>
    </div><?php
}
