<article id="post-<?= $place->getId(); ?>" class="entry-card post-1 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized">

	<h2 class="entry-title">
		<a href="http://localhost:8091/?p=<?= $place->getId(); ?>"><?= $place->getTitle(); ?></a>
	</h2>
    <?php 
    $src = get_the_post_thumbnail_url( $place->getId() ); 
    if($src) {
        $src = str_replace('.jpg','',$src);
        echo get_the_post_thumbnail( $place->getId(), 'medium_large' ); 
    ?>
	
	<?php } ?>
	<div class="entry-excerpt">
    </div>

	<div class="ct-ghost"></div>
    <div class="entry-meta" data-type="simple">							
        <ul>
            <li class="ct-meta-author" itemprop="name">
                <span class="ct-meta-element" itemprop="author" itemscope="itemscope" itemtype="http://schema.org/Person">
						<?= $place->showDrinks($place->data); ?>			
                </span>
            </li>
			
            <li class="ct-meta-date" itemprop="address">
                <span class="ct-meta-element">
                <?= $place->getAddress(); ?>
                </span>
            </li>
			
            <li class="ct-meta-comments">
					
                
            </li>
            
            
			
        </ul>
    </div>
    <?php if(false) { echo $place->dump($src); } ?>
</article>


