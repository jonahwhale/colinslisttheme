<?php

$lat = getVar('lat');
$lat = substr($lat,0,6);
$lng = getVar('lng');
$lng = substr($lng,0,8);

?>
<center>
<div class="card shadow card-search" id="searchCard">
	  <div class="card-body">
          <?php if(is_string($places)) {
              ?><?= $places ?><?php
          } ?>
			<center>
                <form action="/" method="GET">

                <div class="container">
					  <div class="row">
                          <div class="search-group sg-first col-sm top-padding background-white padding-bottom rounded margin-left" >
                              <div class="text-muted"><i>Leave blank for near me</i></div>
                              <?php selectNeighboorhood(); ?>
                              <small class="tiny text-muted">OR</small>
                              <input
                                  type="text"
                                  name="zip"
                                  id="zip"
                                  value="<?= getVar('zip'); ?>"
                                  class="form-control"
                                  placeholder="Near address" />
                            <small class="text-muted tiny">OR</small>
                            <input
                                type="text"
                                name="placeName"
                                id="placeName"
                                value="<?= getVar('placeName'); ?>"
                                class="form-control"
                                placeholder="Place name" />
                          </div>

                            <div class="search-group sg-second col margin-top background-white rounded margin-left">

                                <div class="row colinsListCheckboxWrapper">
                                    <div class="col text-left">
                                        <center class='text-muted'> <i>I can get a drink with my kid</i> </center>
                                    </div>
                                    <div class="col text-right">
                                        <center>
                                        <?php checkbox('colins_list','Colin\'s List'); ?>
                                        </center>
                                    </div>

                                </div>
                                <div class="row parentsNightOutWrapper padding-top">
                                    <div class="col text-left">
                                        <center class='text-muted'> <i>Parent's Night Out </i></center>
                                    </div>
                                    <div class="col text-right">
                                        <center>
                                        <?php checkbox('no_minors','No Minors'); ?>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            <?php if(false) { ?>
                            <div class="search-group col-sm padding-top" style="display:none;">
								<select name="colinsList" class="form-control">
                                    <option>Colin's List</option>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                    <option value="Date">Date Night (No Kids)</option>
                                </select>
							</div>
                            <?php } ?>
                            <div class="search-group sg-third col text-left padding-top padding-bottom margin-top background-white rounded margin-left" >
                                    <?php checkbox('beer','Beer',true); ?>
                                    <?php checkbox('wine','Wine',true); ?>
                                    <?php checkbox('cocktails','Spirits',true); ?>
                                    <?php checkbox('cider','Cider',true); ?>
							</div>
							<!--
							<div class="col">
					      <input type="text" class="form-control" placeholder="Place">
					    </div>
						-->
							<div class="col-sm padding-top">

								<button
                                    type="submit"
                                    class="btn btn-search btn-lg"
                                    id="searchButton"
                                >
                                    <span
                                        id="searching"
                                        class="spinner-border spinner-border-sm"
                                        role="status"
                                        aria-hidden="true"
                                        style="display:none;"
                                    >
                                    </span>
                                    <span id="searchText">Find</span>
                                </button>
						   </div>
                           <!-- <span class="mobile-menu-toggle"></span> -->
			  		  </div> <!-- end row -->
                  </div> <!-- end container -->
                  <input type="hidden" id="lat" name="lat" value="<?= $lat ?>"/>
                  <input type="hidden" id="lng" name="lng" value="<?= $lng ?>"/>

                  </form>
            </center>
		</div>
</div>

<script>
$(document).ready(function() {

    $( "#searchButton" ).click(function() {
        // this.hide();
        $( "#searching" ).show();
        $( "#searchText" ).hide();
    });

    $( "#no_minors" ).change(function() {

        if(this.checked) {
            // $(".sg-third :input").prop('disabled',true);
            // $(".sg-third").children().prop('disabled',true);
            $("#beer").prop("checked",false);
            $("#wine").prop("checked",false);
            $("#cocktails").prop("checked",false);
            $("#cider").prop("checked",false);
            $('.sg-third').fadeOut();
        } else {
            $(".sg-third").fadeIn();
        }
    });
});

</script>
<?php if(!getVar('lat') && !isset($_SESSION['askedGeo'])) {

    ?>
    <script id="getGeo">
    $( document ).ready(function() {
        var lat = $('#lat');
        var lng = $('#lng');
        var debug = false;

        if(!lat.val()) {
            if (navigator.geolocation) {
                if(debug) { alert('hello geo'); }
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                // console.log("Geolocation is not supported by this browser.");
            }
        } else {
            // console.log('Position already determined');
        }

        function showPosition(position) {
            // console.log('hello position');
            // console.log(position.coords.latitude);
            lat = position.coords.latitude.toPrecision(4);
            lng = position.coords.longitude.toPrecision(6);
            // lat = position.coords.latitude.substr(0,6);
            // lng = position.coords.longitude.substr(0,8);

            window.location.href = '?lat='+lat+'&lng='+lng;
        }

    });
    </script>
    <?php
    $_SESSION['askedGeo'] = 1;
}
