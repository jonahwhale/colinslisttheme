== TODO ==

Redo location cards setup per previously sent modifications (see below for image again)
Moving alcohol symbols to upper left - I don't know where the new distance and neighborhoods should go under location search though...
Add high chair and changing table symbols on the upper right
Make the address font size smaller
Move the "tip" to below the picture and italicize
Add in the share tip/correction button, which will hopefully generate a pop-out
form similar to a contact us, that auto-populates the location name as the subject or something like that.

Yellow check symbol for locations with, "yes but caviat".
I would like these locations to show up on both Colin's List & No Minors check box options.
If anything additional is needed to be added to the location details entered to make it happen, no problem.
Geolocated search option in addition to entering address

Tip paragraphing/spacing:
right now it all blends together, but I want to be able to do separate lines for multiple tips
Adding in Changing Table & High Chair symbols

Click on address needs to open in new window
Neighborhood drop-down list on desktop view mashes words together if the line is too long, needs to either be given more space or default to 2 lines
Social Media links added to page footer:
www.facebook.com/colinslist
www.twitter.com/colinslist
www.instagram.com/colinslist
It seems like Adsense just won't work.  I found an alternative - media.net - that may be a good option.  I think it's best to finalize the site and do this last as adding as that was the initial issue with Adsense.
The options check-boxes and any highlighting done is green, pls make it blue
When a search is conducted on my phone, it's not obvious results are there because it only shows the search bar.  Pls make it so that after FIND is clicked, the page view starts lower, at the first result.
The neighborhood name only shows up in address-based searches and the box is too small to fit the whole neighborhood.  This either needs to be eliminated or made to size properly
And a couple of other things:
The beer/wine/spirits/cider symbols are different gray shades, pls make the same, and should be the same as the social symbols
Change top slogan to: Where can "I" still...
Contact Us page:
Please add the following paragraph before the contact form:  "Colin's List is the creation of one working mom.  Your help editing and adding location information is essential to our common sanity and battle against parental loneliness.  Please contact us with helpful information to keep Colin's List up to date.  You can fill out the form below or email us at hello@colinslist.info. Thanks!"



=== Blocksy ===
Contributors: creativethemeshq
Website: https://colinslist.info
Email: jonahb0001@gmail.com
Tags: blog, e-commerce, wide-blocks, block-styles, grid-layout, one-column, two-columns, three-columns, four-columns, right-sidebar, left-sidebar, translation-ready, custom-colors, custom-logo, custom-menu, featured-images, footer-widgets, full-width-template, theme-options, threaded-comments
Requires at least: 5.2
Requires PHP: 5.7
Tested up to: 5.2
Stable tag: trunk
License: GNU General Public License v2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Blocksy is a blazing fast and lightweight WordPress theme built with the latest web technologies. It was built with the Gutenberg editor in mind and has a lot of options that makes it extendable and customizable. You can easily create any type of website, such as business agency, shop, corporate, education, restaurant, blog, portfolio, landing page and so on. It works like a charm with popular WordPress page builders, including Elementor, Beaver Builder, Visual Composer and Brizy. Since it is responsive and adaptive, translation ready, SEO optimized and has WooCommerce built-in, you will experience an easy build and even an increase in conversions.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Type in Blocksy in the search form and press the 'Enter' key on your keyboard.
3. Click Activate to use your new theme right away.

== Build Instructions ==

This theme contains some JavaScript files that need a compilation step in
order to be consumable by the browsers. The compilation is done by the
[`build-process`](https://github.com/creative-Themes/build-process) package
(which is just a preset config over WebPack). We do plan to eventually migrate
over to [`wp-scripts`](https://github.com/WordPress/gutenberg/tree/master/packages/scripts)
as the build pipeline.

So, in order to build the theme files, you need to execute these commands:

1. `npm install` or `yarn install`, both work just fine.
2. `npm run build` -- for a production build, or `npm run dev` for developments builds, which include a proper file watcher
3. The final files will be included in `admin/dashboard/static/bundle` and `static/bundle` directories.

The repeated `BlocksyReact`, `BlocksyReactDOM` and `wp.element` got enqueued
that way for two reasons:

1. We started to use React Hooks in WordPress 5.1, and we needed an actual
version of `wp.element` for that. A version of `wp.element` with hooks got
shipped only with WordPress 5.2. We planned on getting rid of our global version as
soon as 5.2 got released, but now I see a problem with backwards
compatibility.
2. We need to use a global version of React and ReactDOM because we have some
components using hooks both in the theme an din the `blocksy-companion` plugin
(https://creativethemes.com/downloads/blocksy-companion.zip). That way we
avoid breaking the rules of hooks.

== Screenshot Licenses ==

Screenshot images are all licensed under CC0 Public Domain
http://streetwill.co/posts/749-az4
http://streetwill.co/posts/788-black-day
http://streetwill.co/posts/205-peaceful
http://streetwill.co/posts/497-coloration
http://streetwill.co/posts/811-grass-flower-on-sunset-background
http://streetwill.co/posts/454-camber-sands-beach-house
http://streetwill.co/posts/350-golden
http://streetwill.co/posts/610-food5
http://streetwill.co/posts/853-aucstp
