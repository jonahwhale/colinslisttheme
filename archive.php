<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Blocksy
 */

global $startTime;
$startTime = microtime(true);
get_header();

$debug = getVar('debug');

$listing_source = blocksy_get_posts_listing_source();

$blog_post_structure = blocksy_akg_or_customizer(
	'structure',
	$listing_source,
	'grid'
);

$blog_post_columns = blocksy_akg_or_customizer(
	'columns',
	$listing_source,
	'3'
);

$card_type = blocksy_akg_or_customizer(
	'card_type',
	$listing_source,
	'boxed'
);

$columns_output = '';

if ( $blog_post_structure === 'grid' ) {
	$columns_output = 'data-columns="' . $blog_post_columns . '"';
}

?>

<?php

/**
 * Note to code reviewers: This line doesn't need to be escaped.
 * Function blocksy_output_hero_section() used here escapes the value properly.
 */
echo blocksy_output_hero_section( 'type-2' );
// get all places
if($debug) {
    showTime($startTime,'Pre get places');
}

$places = get_places();

if($debug) {
    showTime($startTime,'Post get places');
}
$showAll = (int)getVar('showAll');
// Column limit
if($showAll) {
    $rowLimit = 300;
} else {
    $rowLimit = 20;
}

$zip = getVar('zip');
$lat = getVar('lat');
$lng = getVar('lng');

if($zip) {
	$pos = getPosition($zip);
    if($debug) dump($pos);
	$places = placeDistance($places,$pos->lat,$pos->lng);
} elseif($lat) {
    $_SESSION['lat'] = $lat;
    $_SESSION['lng'] = $lng;
    // dump($places);
    $places = placeDistance($places,$lat,$lng);
}

if($debug) {
    showTime($startTime,'Places post geo');
}
?>

<section id="primary" class="content-area">
    <?php require(dirname(__FILE__)."/template-parts/search.php"); ?>

	<div class="ct-container" <?php echo wp_kses_post(blocksy_sidebar_position_attr()); ?>>
		<section>
			<?php if ( $places ) { ?>
				<?php
					/**
					 * Note to code reviewers: This line doesn't need to be escaped.
					 * Function blocksy_output_hero_section() used here
					 * escapes the value properly.
					 */
					// echo blocksy_output_hero_section( 'type-1' );
				?>

				<?php if ( is_array($places) ) { ?>
				    <center>
                        <?php require(dirname(__FILE__).'/template-parts/place-cards.php'); ?>
					</center>
			<?php }


				/**
				 * Note to code reviewers: This line doesn't need to be escaped.
				 * Function blocksy_display_posts_pagination() used here escapes the value properly.
				 */
				// echo blocksy_display_posts_pagination();
			} else {
				get_template_part( 'template-parts/content', 'none' );
			}

			?>

		</section>

		<?php // get_sidebar(); ?>
	</div>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-format="fluid"
         data-ad-layout-key="-fb+5w+4e-db+86"
         data-ad-client="ca-pub-8867570642175238"
         data-ad-slot="1608750827"></ins>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</section>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
<?php
if($debug) {
    showTime($startTime,'Places Shown');
}

get_footer();
