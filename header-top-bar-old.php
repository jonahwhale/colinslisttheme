<header
		class="site-header"
		<?php blocksy_schema_org_definitions_e('header') ?>
		<?php echo wp_kses_post($stacking_output) ?>
		<?php echo wp_kses_post($transparent_border_output) ?>>
		<?php
		    
		if (
				blocksy_has_top_bar() || (
					isset( $for_preview ) && $for_preview
				)
			) {
				/**
				 * Note to code reviewers: This line doesn't need to be escaped.
				 * Function blocksy_output_header_top_bar() used here escapes the value properly.
				 */
				echo blocksy_output_header_top_bar();
			}
		?>

		<?php get_template_part( $template ); ?>
		<?php get_template_part( 'template-parts/header/mobile' ); ?>
		
	</header>