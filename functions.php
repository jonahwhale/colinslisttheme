<?php
/**
 * Blocksy functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ColinsList
 */



if ( version_compare( PHP_VERSION, '5.7.0', '<' ) ) {
    require get_template_directory() . '/inc/php-fallback.php';
    return;
}

require get_template_directory() . '/inc/init.php';

add_action( 'wp_footer', 'contact_wp_footer' );

// https://developer.wordpress.org/rest-api/extending-the-rest-api/adding-custom-endpoints/
add_action( 'rest_api_init', function () {
    register_rest_route( 'places/v1', '/author/(?P<id>\d+)', array(
        'methods' => 'GET',
        'callback' => 'get_places_json',
    ) );
} );

function sess_start() {
    if (!session_id())
    session_start();
}

add_action('init','sess_start');


/**
 *
 */

function placeDistance($places,$lat,$lng) {
    // dump($pos);
    $debug = false;
    $pos = new stdclass();
    $pos->lat = $lat;
    $pos->lng = $lng;
    // dump($pos);
	foreach($places as $place) {
        // dump($place);
        //$ll = $place->getLatLng();
        $ll = $place->lat_lng;
        if($debug) dump($ll);

        $d = getDistance($pos,$ll);
        // dump($d);
        // dump($d->rows[0]->elements[0]->distance->text);
        // $place->setDistance($d);
        $place->distance = $d;
        $place->data->distance = $d;
    }
    usort($places, "sortDistance");
    return $places;
}

function selectNeighboorhood() {
    // wp_dropdown_pages(array('post_type'=>'neighborhood'));
    generate_post_select('n','neighborhood');
    /*
    ?>
        <select name="n" id="n" class="form-control">
            <option>Neighborhood</option>
            <option value="Ballard">Ballard</option>
            <option value="West Seattle">West Seattle</option>
            <option value="Greenwood/Phinney Ridge">Greenwood/Phinney Ridge</option>
            <option value="Date">More to come</option>
        </select>

    <?php
    */
}

function getPlaces() {
    $sql = "select * from wp_ftlmg9bccy_pods_place where drinks like '%beer%' ";
}

/**
 * Search logic
 */
function get_places($all=false) {

    global $startTime;
    $pt = new PlaceTable();
    $debug = getVar('debug');
    $n = getVar('n');
    $zip = getVar('zip');
    $lat = getVar('lat');
    $lng = getVar('lng');
    $newCache = getVar('cache');
    $placeName = getVar('placeName');
    $noMinors = getVar('no_minors');
    $colinsList = getVar('colins_list');

    $drinks = array();
    $drinks['beer'] = getVar('beer');
    $drinks['wine'] = getVar('wine');
    $drinks['cider'] = getVar('cider');
    $drinks['cocktails'] = getVar('cocktails');

    $drinksSelected = false;
    foreach($drinks AS $selected) {
        if($selected) {
            $drinksSelected = true;
        }
    }

    $pl = array();
    /*
    $params = array(
        'limit' => 3,
        'page' => 2,
        // Be sure to sanitize ANY strings going here
        'where'=>"category.name = 'My Category'"

        $params = array(
        'where' => 't.post_title LIKE "%' . $keyword . '%" OR my_field.meta_value LIKE "%' . $keyword . '%"'
        );
    );
    */
    $params = ['limit'=>2000];
    // Run the find

    $mypod = pods( 'place', $params );
    if($debug) {
        showTime($startTime,'Post pods place');
    }

    if($placeName) {
        // $placeName = sanitize_text_field($placeName);

        $params = array(
            'where' => 't.post_title LIKE "%' . $placeName . '%" '
        );

        $mypod->find($params);
    }

    // unset($_SESSION['places']);

    $pods = $mypod->data();

    if(!is_array($pods)) {
        return 'Nothing found';
    }

    $places = [];
    // $places = wp_cache_get('allPlaces');

    // $places_json = file_get_contents('places.json');
    // print_r($places_json);
    // $placesCache = json_decode($places_json,true);
    // print_r($places);
    $newCache = getVar('newCache');
    if($newCache) {
        $places = $pt->resetCache($places);
    } else {
        $places = $pt->getCache();
    }



    if(!$places) {
        if($debug) dump('allPlaces cache miss');
        foreach($pods AS $pod) {
            // This is where things get slow.
            $p = new Place($pod);
            unset($p->_pod);
            // print_r(json_encode($p));
            $places[$p->getID()] = $p;
            // dump(get_object_vars($p));
            // $placesArray[$p->getID()] = get_object_vars($p);
             // json_encode($response->response->docs), true);
            // $placeJson = json_encode($p);
            // $places[$p->getID()] = $placeJson;
            // $array = json_decode(json_encode($p), true);
            // dump($array);
        }
        $pt->putCache($places);
        // wp_cache_set('allPlaces',$places);

        // file_put_contents('places.json',json_encode($placesArray));
    }


    /*
    $places = array_map(
        function($key, $value) { return array($key, $value); },
        array_keys($places),
        array_values($places)
    );
    */
    // file_put_contents('places.json',json_encode($placesArray,true));
    // dump(json_encode($places));
    // file_put_contents('places.json',json_encode($places));
    // dump($places);

    // dump($places);
    foreach($places AS $p) {
        /*
        $p = (object) $p;
        $p->data = (object) $p->data;
        */
        // dump($p);
        // Hide places without names.
        if($placeName) {
            $show = true;
        } else {
            $show = false;
        }
        // $p->dump();

        if($n) {
            if($p->neighborhood_id == $n) {
                $show = true;
            }
        } elseif($zip || $lat) {
            // zipcode
            $show = true;
        }

        if($show && $noMinors) {
            // da($place->why_not_caveat);
            $show = false;
            /*
            if($p->why_not_caveat) {
                $show = true;
            }
            */

            /**
             * Shows no minors or any place with a why_not_caveat
             */

            if
            (
                strpos(strtolower($p->why_not_caveat),strtolower('o Minors'))
                || ($p->why_not_caveat && $p->why_not_caveat!='No Alcohol')
            ) {
                $show = true;
            }



        } elseif($show && $colinsList) {
            if(!$p->data->colins_list) {
                $show = false;
                continue;
            }
        }

        if($drinksSelected) {
            $drinkHit = false;
            foreach($drinks as $drink => $showDrink) {
                if($showDrink) {
                    $d = strtolower($p->data->drinks);

                    if(strpos($d,$drink)) {
                        $drinkHit = true;

                    } else {

                    }

                }
            }
            if(!$drinkHit) { $show = false; }
        }
        if($show) {
            $pl[$p->id] = $p;
        }
    }
    // print_r($pl);
    if($all && !count($pl)) {
        return $places;
    } else {
        return $pl;
    }
}

function generate_post_select($select_id,$post_type, $selected = 0) {
    if(isset($_REQUEST[$select_id])) {
        $selected = $_REQUEST[$select_id];
    }
    $post_type_object = get_post_type_object($post_type);
    $label = $post_type_object->label;
    $posts = get_posts(
        array(
            'post_type'=> $post_type,
            'post_status'=> 'publish',
            'suppress_filters' => false,
            'posts_per_page'=>-1,
            'orderby'=> 'title',
            'order' => 'ASC'
        )
    );
    echo '<select name="'. $select_id .'" id="'.$select_id.'" class="form-control">';
    // echo '<option value = "" >All '.$label.' </option>';
    echo '<option value = "" >Choose '.$post_type.' </option>';

    foreach ($posts as $post) {
        echo '<option value="', $post->ID, '"', $selected == $post->ID ? ' selected="selected"' : '', '>', $post->post_title, '</option>';
    }
    echo '</select>';
}

function checkbox($field,$label,$defaultSelected=false) {
    $pt = new PlaceTable();
    $selected = getVar($field);

    if($selected) {
        $checked = 'checked';
    } else {
        $checked = '';
    }

    ?>
    <div class="form-check">
        <nobr>
            <input
                class="form-check-input"
                type="checkbox"
                value="1"
                id="<?= $field ?>"
                name="<?= $field ?>"
                <?= $checked ?>
                >
            <label class="form-check-label" for="<?= $field ?>">
                &nbsp;
                <?php switch($field) {
                    case"cider":
                    case"wine":
                    case"spirits":
                    case"cocktails":
                    case"beer":
                        $pt->showIcon($field);

                } ?>
                <?= $label ?>
            </label>
        </nobr>
    </div>
    <?php
}

function contact_wp_footer() {
    ?>
    <script type="text/javascript">
    document.addEventListener( 'wpcf7mailsent', function( event ) {
        window.location.href = "https://colinslist.info?messageSent=1";
    }, false );
    </script>
    <?php
}

function showTime($start,$label=false) {
    $time_post = microtime(true);
    $exec_time = $time_post - $start;
    dump($label);
    dump($exec_time);
}


function colins_cron_schedule( $schedules ) {
    $schedules['every_six_hours'] = array(
        'interval' => 21600, // Every 6 hours
        'display'  => __( 'Every 6 hours' ),
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'colins_cron_schedule' );

//Schedule an action if it's not already scheduled
if ( ! wp_next_scheduled( 'colins_cron_hook' ) ) {
    wp_schedule_event( time(), 'every_six_hours', 'colins_cron_hook' );
}

///Hook into that action that'll fire every six hours
 add_action( 'colins_cron_hook', 'refresh_cache' );

//create your function, that runs on cron
function refresh_cache() {
    file_get_contents('https://colinslist.net/?newCache=1');
}

function showInlineAd() {
    ?>
    
    <ins class="adsbygoogle"
         style="display:block"
         data-ad-format="fluid"
         data-ad-layout-key="-fb+5w+4e-db+86"
         data-ad-client="ca-pub-8867570642175238"
         data-ad-slot="1608750827"></ins>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
    <?php
}
