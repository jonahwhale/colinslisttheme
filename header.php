<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ColinsList
 */

$header_type = get_theme_mod( 'header_type', 'type-1' );

$template = 'template-parts/header/' . (
	str_replace('type', 'header', $header_type)
);

ob_start();

if ( is_customize_preview() ) {
	$for_preview = true;
	include locate_template( 'template-parts/header/header-1.php' );
	$for_preview = false;
}

$type_1_output = ob_get_clean();

ob_start();

if ( is_customize_preview() ) {
	$for_preview = true;
	include locate_template( 'template-parts/header/header-2.php' );
	$for_preview = false;
}

$type_2_output = ob_get_clean();

blocksy_add_customizer_preview_cache(
	function () use ($type_1_output, $type_2_output) {
		return blocksy_html_tag(
			'div',
			[ 'data-id' => 'header' ],
			blocksy_html_tag( 'div', [ 'data-type' => 'type-1' ], $type_1_output ) .
			blocksy_html_tag( 'div', [ 'data-type' => 'type-2' ], $type_2_output )
		);
	}
);

blocksy_add_customizer_preview_cache(
	function () {
		if (blocksy_has_custom_top_bar()) {
			return '';
		}

		return blocksy_html_tag(
			'div',
			[ 'data-id' => 'header-top-bar' ],
			blocksy_output_header_top_bar()
		);
	}
);

blocksy_header_top_bar_sections_cache();

$transparent_border_output = '';

$header_bottom_border = get_theme_mod('headerBottomBorder', [
	'width' => 1,
	'style' => 'none',
	'color' => [
		'color' => 'rgba(18, 21, 25, 0.98)',
	],
]);

if (blocksy_akg('style', $header_bottom_border) !== 'none') {
	$transparent_border_output = 'data-border="' . (
		get_theme_mod('headerBottomBorderContainer', 'no') === 'yes' ? 'full-width' : 'contained'
	) . '"';
}

$stacking_output = '';

if (blocksy_has_top_bar()) {
	if (
		get_theme_mod(
			'header_top_bar_section_1',
			'header_menu'
		) !== 'disabled' && get_theme_mod(
			'header_top_bar_section_2',
			'disabled'
		) !== 'disabled'
	) {
		$stacking_output = blocksy_stacking(
			get_theme_mod('top_bar_stacking', [
				'tablet' => false,
				'mobile' => true,
			]),
			'data-top-bar-stack'
		);
	}
}


$transparent_output = '';

if (blocksy_has_transparent_header()) {
	$transparent_output = blocksy_stacking(
		get_theme_mod('transparent_header_visibility', [
			'desktop' => true,
			'tablet' => true,
			'mobile' => false,
		]),
		'data-transparent-header'
	);
}

if (is_customize_preview()) {
	if (blocksy_header_has_custom_transparency()) {
		blocksy_add_customizer_preview_cache(
			blocksy_html_tag(
				'div',
				['data-transparent-header-custom' => blocksy_header_get_transparency()],
				''
			)
		);
	}

	if (!blocksy_has_transparent_header(true)) {
		blocksy_add_customizer_preview_cache(
			blocksy_html_tag(
				'div',
				['data-transparent-forced-by-checkboxes' => ''],
				''
			)
		);
	}
}

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta name="google-site-verification" content="JqBIXdWxrt1znCTg6Ea0El8D0xNO_9PisdppEsZ4cIw" />

	<link rel="profile" href="https://gmpg.org/xfn/11">


	<?php if($flat=true) { ?>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/yeti/bootstrap.min.css" rel="stylesheet" integrity="sha384-w6tc0TXjTUnYHwVwGgnYyV12wbRoJQo9iMlC2KdkdmVvntGgzT9jvqNEF/uKaF4m" crossorigin="anonymous">
	<?php } else { ?>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
	<?php } ?>

    <?php wp_head(); ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
   <link href="https://fonts.googleapis.com/css?family=Montserrat:900|Work+Sans:300|Roboto:200,400,900" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Fira+Sans:400,600,800&display=swap" rel="stylesheet">
   <link href="https://fonts.googleapis.com/css?family=Lato:400,900&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
   <?php require(dirname(__FILE__)."/template-parts/adsense.php"); ?>
</head>

<body <?php body_class(); ?> <?php echo wp_kses_post($transparent_output) ?>>

<script   src="https://code.jquery.com/jquery-3.4.1.min.js"   integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="   crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>

<div id="main-container">
	<?php require(dirname(__FILE__)."/header-top-bar.php"); ?>

	<main id="main" class="site-main">


</center>
		<?php if (function_exists('blc_output_read_progress_bar')) { ?>
			<?php
				/**
				 * Note to code reviewers: This line doesn't need to be escaped.
				 * Function blc_output_read_progress_bar() used here escapes the value properly.
				 */
				echo blc_output_read_progress_bar()
			?>
		<?php } ?>
